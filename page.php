<!doctype html>
<html lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>
        <div class="o-wrapper" id="o-wrapper">
        <div class="wrapper">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main clearfix">

                <!-- Sidebar Left -->
                <?php include('inc/sidebar_left.inc.php') ?>
                <!-- -->


                <div class="right_block_vp"><!-- header banner fonbet -->

                    <!-- SIDEBAR Right -->
                    <?php include('inc/sidebar_right.inc.php') ?>
                    <!-- -->

                    <article>

                        <div class="page">

                            <div class="page__box">
                                <ul class="forecast_nav">
                                    <li><a href="#"><span>Мои ставки</span></a></li>
                                    <li><a href="#"><span>Общий рейтинг</span></a></li>
                                    <li><a href="#"><span>Текущие ставки</span></a></li>
                                    <li><a href="#"><span>Обсуждение</span></a></li>
                                </ul>
                            </div>

                            <div class="page__box">

                                <div class="forecaster">
                                    <div class="forecaster__subscribe">
                                        <div class="forecaster__avatar">
                                            <img src="img/avatar.jpg" class="img-fluid" alt="">
                                        </div>
                                        <a href="#" class="forecaster__name">БЕН.БРУНОВИЧ</a>
                                        <div class="forecaster__status">Чемпионы VPROGNOZE</div>

                                        <ul class="btn-group">
                                            <li><a href="#order" class="btn btn-green btn-modal">Купить подписку</a></li>
                                            <li><a href="#" class="btn btn-white">Отправить сообщение</a></li>
                                        </ul>
                                    </div>
                                    <div class="forecaster__info">
                                        <div class="forecaster__info_label">Предыдущие итоги:</div>
                                        <form class="form">
                                            <div class="forecaster__info_row">
                                                <div class="forecaster__info_input">
                                                    <select class="form-select form-control-white" name="cid">
                                                        <option value="201307">1. 2013 / Июль</option>
                                                        <option value="201308">2. 2013 /Август</option>
                                                        <option value="201309">3. 2013 / Сентябрь</option>
                                                        <option value="201310">4. 2013 / Октябрь</option>
                                                        <option value="201311">5. 2013 /Ноябрь</option>
                                                        <option value="201312">6. 2013 / Декабрь</option>
                                                        <option value="201401">7.2014 / Январь</option>
                                                        <option value="201402">8. 2014 / Февраль</option>
                                                        <option value="201403">9. 2014 / Март</option>
                                                        <option value="201404">10. 2014 /Апрель</option>
                                                        <option value="201405">11. 2014 / Май</option>
                                                        <option value="201406">12.2014 / Июнь</option>
                                                        <option value="201407">13. 2014 / Июль</option>
                                                    </select>
                                                </div>
                                                <div class="forecaster__info_button">
                                                    <button class="btn btn-blue" type="submit">Показать</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="indicators open">
                                    <div class="indicators__heading">
                                        <i class="indicators__toggle"></i>
                                        <span>Показатели прибыли прогнозиста</span>
                                    </div>
                                    <div class="indicators__body">
                                        <div class="indicators__content">

                                            <div class="indicators__elem">
                                                <div class="indicators__elem_wrap">
                                                    <div class="indicators__title">Текущий месяц</div>
                                                    <div class="indicators__table">
                                                        <ul>
                                                            <li><span>Прибыль</span></li>
                                                            <li><strong class="text-green">+26.54% (+796)</strong></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Выигрышей</span></li>
                                                            <li><span>25</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Проигрышей</span></li>
                                                            <li><span>15</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Проходимость</span></li>
                                                            <li><span class="text-blue">63%</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>ROI</span></li>
                                                            <li><span>14%</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><strong>Средний кэф</strong></li>
                                                            <li><strong class="text-green">1.86</strong></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="indicators__footer">Текущий месяц</div>
                                            </div>

                                            <div class="indicators__elem">
                                                <div class="indicators__elem_wrap">
                                                    <div class="indicators__title">Текущий месяц</div>
                                                    <div class="indicators__table">
                                                        <ul>
                                                            <li><span>Прибыль</span></li>
                                                            <li><strong class="text-green">+26.54% (+796)</strong></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Выигрышей</span></li>
                                                            <li><span>25</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Проигрышей</span></li>
                                                            <li><span>15</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>Проходимость</span></li>
                                                            <li><span class="text-blue">63%</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><span>ROI</span></li>
                                                            <li><span>14%</span></li>
                                                        </ul>
                                                        <ul>
                                                            <li><strong>Средний кэф</strong></li>
                                                            <li><strong class="text-green">1.86</strong></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="indicators__footer open">За все время</div>
                                            </div>

                                        </div>

                                        <div class="indicators__graph">
                                            <img src="img/graph.png" class="img-fluid" alt="">
                                        </div>

                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="myTable">
                                        <tr class="myTable_header">
                                            <td colspan="6">Показатели прибыли за последние 6 месяцев</td>
                                        </tr>
                                        <tr class="myTable_title">
                                            <th>Месяц</th>
                                            <th>Прибыль</th>
                                            <th>WIN</th>
                                            <th>LOSE</th>
                                            <th>Проходимость</th>
                                            <th>Ср.кф</th>
                                        </tr>
                                        <tr>
                                            <td>2018-05</td>
                                            <td><span class="text-green"> +5.97% (+179)</span></td>
                                            <td>2</td>
                                            <td>0</td>
                                            <td><span class="text-green">100.0%</span></td>
                                            <td><span class="text-green">1.90</span></td>
                                        </tr>
                                        <tr>
                                            <td>2018-05</td>
                                            <td><span class="text-green"> +66.27% (+1988)</span></td>
                                            <td>69</td>
                                            <td>44</td>
                                            <td><span class="color-blue">61.1%</span></td>
                                            <td><strong  class="text-green">1.99</strong></td>
                                        </tr>
                                        <tr>
                                            <td>2018-03</td>
                                            <td><span class="text-green"> +28.90% (+867)</span></td>
                                            <td>44</td>
                                            <td>39</td>
                                            <td><span class="text-red">53.0%</span></td>
                                            <td><strong class="text-green">2.02</strong></td>
                                        </tr>
                                        <tr>
                                            <td>2018-05</td>
                                            <td><span class="text-green"> +5.97% (+179)</span></td>
                                            <td>2</td>
                                            <td>0</td>
                                            <td><span class="text-green">100.0%</span></td>
                                            <td><span class="text-green">1.90</span></td>
                                        </tr>
                                        <tr>
                                            <td>2018-05</td>
                                            <td><span class="text-blue"> +66.27% (+1988)</span></td>
                                            <td>69</td>
                                            <td>44</td>
                                            <td><span class="color-blue">61.1%</span></td>
                                            <td><strong  class="text-green">1.99</strong></td>
                                        </tr>
                                        <tr>
                                            <td>2018-03</td>
                                            <td><span class="text-green"> +28.90% (+867)</span></td>
                                            <td>44</td>
                                            <td>39</td>
                                            <td><span class="text-blue">53.0%</span></td>
                                            <td><strong class="text-green">2.02</strong></td>
                                        </tr>
                                    </table>

                                </div>

                                <a class="btn-still">
                                    <span class="btn-still-text"><span>Показать ещё...</span><i></i></span>
                                </a>

                            </div>

                            <div class="page__box">
                                <!-- Subscribe -->
                                <div class="subscribeBlock">
                                    <div class="subscribeBlock__heading">Выберите подписку</div>
                                    <div class="subscribeBlock__row">

                                        <div class="subscribeBlock__item">
                                            <div class="subscribeBlock__item_title">1 день</div>
                                            <div class="subscribeBlock__item_content">
                                                <div class="subscribeBlock__item_price">500 руб.</div>
                                                <div class="subscribeBlock__item_sale">30% экономия</div>
                                            </div>
                                            <div class="subscribeBlock__item_button">
                                                <a class="btn btn-green">Оформить</a>
                                            </div>
                                        </div>

                                        <div class="subscribeBlock__item">
                                            <div class="subscribeBlock__item_title">3 дня</div>
                                            <div class="subscribeBlock__item_content">
                                                <div class="subscribeBlock__item_price">1000 руб.</div>
                                                <div class="subscribeBlock__item_sale">30% экономия</div>
                                            </div>
                                            <div class="subscribeBlock__item_button">
                                                <a class="btn btn-green">Оформить</a>
                                            </div>
                                        </div>

                                        <div class="subscribeBlock__item">
                                            <div class="subscribeBlock__item_title">1 день</div>
                                            <div class="subscribeBlock__item_content">
                                                <div class="subscribeBlock__item_price">7 дней</div>
                                                <div class="subscribeBlock__item_sale">30% экономия</div>
                                            </div>
                                            <div class="subscribeBlock__item_button">
                                                <a class="btn btn-green">Оформить</a>
                                            </div>
                                        </div>

                                        <div class="subscribeBlock__item">
                                            <div class="subscribeBlock__item_title">14 дней</div>
                                            <div class="subscribeBlock__item_content">
                                                <div class="subscribeBlock__item_price">1500 руб.</div>
                                                <div class="subscribeBlock__item_sale">30% экономия</div>
                                            </div>
                                            <div class="subscribeBlock__item_button">
                                                <a class="btn btn-green">Оформить</a>
                                            </div>
                                        </div>

                                        <div class="subscribeBlock__item">
                                            <div class="subscribeBlock__item_title">21 день</div>
                                            <div class="subscribeBlock__item_content">
                                                <div class="subscribeBlock__item_price">7000 руб.</div>
                                                <div class="subscribeBlock__item_sale">30% экономия</div>
                                            </div>
                                            <div class="subscribeBlock__item_button">
                                                <a class="btn btn-green">Оформить</a>
                                            </div>
                                        </div>

                                        <div class="subscribeBlock__item">
                                            <div class="subscribeBlock__item_title">30 дней</div>
                                            <div class="subscribeBlock__item_content">
                                                <div class="subscribeBlock__item_price">500 руб.</div>
                                                <div class="subscribeBlock__item_sale">30% экономия</div>
                                            </div>
                                            <div class="subscribeBlock__item_button">
                                                <a class="btn btn-green">Оформить</a>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <!-- -->

                                <ul class="list-check">
                                    <li>Прогнозы рассылаются на почту, в ЛС, СМС (если телефон указан в профиле)</li>
                                    <li>Подписка продлевается на 1 день, если не было дано ни одного прогноза</li>
                                    <li>Eсли игровой день был в минусе, то на каждый минусовой прогноз выдается купон (для подписок без гарантии)</li>
                                    <li>С гарантией возврата - если прибыль прогнозиста отрицательная по прогнозам из интервала Вашей подписки - Мы возвращаем Вам 85% стоимости подписки.</li>
                                </ul>
                            </div>

                            <div class="page__box">
                                <h3 class="text-center">Результаты за предыдущие дни</h3>
                                <div class="table-responsive">
                                    <table class="myTable table-forecast">
                                        <tr class="myTable__title">
                                            <th>БК</th>
                                            <th>Дата</th>
                                            <th>Наименование</th>
                                            <th>Прогноз</th>
                                            <th>Сумма</th>
                                            <th>Итог</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_betcity.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td>
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td class="color-green">2.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_marathon.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td class="table-forecast-dropdown">
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td class="color-green">2.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_marathon.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td class="table-forecast-dropdown">
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td>2.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_marathon.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td>
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td>2.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_marathon.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td>
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td class="color-blue">2.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_marathon.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td>
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td>2.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_marathon.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td>
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td>2.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_marathon.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td>
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td>2.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_marathon.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td>
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td>2.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_marathon.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td>
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td>2.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_marathon.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td>
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td>2.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_marathon.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td>
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td>2.0</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <IMG width="16" alt="Betcity" src="img/mini_marathon.png">
                                                </a>
                                            </td>
                                            <td>
                                                <div>0-04</div>
                                                <div>20:45</div>
                                            </td>
                                            <td>
                                                <strong class="game_name">Д.Хазанюк - Д.Елбаба</strong>
                                                <span class="game_info">Теннис / Турнир ITF. США. Дотан. Женщины. Одиночный разряд. Квалификация. 2-й раунд</span>
                                            </td>
                                            <td>ФОРА1 по геймам (-5) @ 1.85<sup class="text-red">-5.5</sup></td>
                                            <td>171</td>
                                            <td>2.0</td>
                                        </tr>
                                    </table>
                                </div>
                                <a class="btn-still">
                                    <span class="btn-still-text"><span>Показать ещё...</span><i></i></span>
                                </a>
                            </div>

                            <div class="page__box">
                                <!-- Способы оплаты -->
                                <div class="box_wrap">
                                    <div class="box_heading">Способы оплаты</div>

                                    <div class="paymentMethod">
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_01.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_02.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_03.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_04.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_05.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_06.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_07.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_08.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_09.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_10.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_11.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_12.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_13.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="paymentMethod__elem">
                                            <img src="img/payment/pay_icon_14.jpg" class="img-fluid" alt="">
                                        </div>
                                    </div>

                                    <ul class="list-check">
                                        <li><strong>Webmoney</strong> (WMR рубли, WMZ доллары, WMU гривны, WME евро) </li>
                                        <li><strong>SPRAYPAY</strong> (Яндекс Деньги, QIWI Кошелек, MoneyMail, денежные переводы, Почта России, Сбербанк, моб. телефоны)</li>
                                        <li><strong>RBK Money</strong> (VISA, MasterCard, МИР, Евросеть, Альфа-клик, Элекснет, Contact, Rapida)</li>
                                    </ul>

                                </div>
                                <!-- -->
                            </div>

                            <div class="page__box">
                                <div class="box_wrap">
                                    <div class="box_heading">Купить прогнозы на спорт на Впрогнозе это выгодно! Почему?</div>
                                    <ul class="forecast">
                                        <li>Наши прогнозисты доказали свою прибыльность сотнями бесплатных прогнозов на спорт на Впрогнозе</li>
                                        <li>В платный раздел попадают самые лучшие прогнозы на матчи от наших экспертов</li>
                                        <li>Простая процедура оплаты спортивных прогнозов, системы скидок и бонусов для постоянных клиентов</li>
                                        <li>Доступ на промежуток времени подразумевает доступ ко всем прогнозам выбранного прогнозиста на определнное количество часов или дней</li>
                                        <li>Уведомление на почту, СМС, на сайт в личку</li>
                                        <li>Минимальный кф 1.6, максимальный кф. 5, матчи по линии БК Бетсити, Марафон, SBOBET, Bet365, экспрессы исключительно по Марафону</li>
                                    </ul>

                                    <div class="warning">
                                        <div class="warning__alert"><span>Внимание!</span></div>
                                        <div class="warning__text">Администрация сайта не гарантирует 100% исход прогнозов и не несет ответственности за финансовые потери игроков</div>
                                    </div>

                                    <div class="site_contact">По всем вопросам пишите сюда:  <a href="#">Администратору</a></div>
                                    <ul class="btn-group centered">
                                        <li><a href="#" class="btn">Сведения о компании</a></li>
                                        <li><a href="#" class="btn">Условия оплаты и возврата</a></li>
                                        <li><a href="#" class="btn">Безопасность платежей</a></li>
                                    </ul>

                                </div>
                            </div>

                        </div>

                    </article>

                </div>

                <div class="clr"></div>
                <div id="dialog-form"></div>

            </section>
            <!--noindex-->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>
    </div>

    <!-- upd 01.05.2018 -->

        <!-- Modal  -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts  -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
