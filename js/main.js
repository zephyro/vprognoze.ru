
$('.indicators__heading').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.indicators').toggleClass('open');
});


$(".btn-modal").fancybox({
    'padding'    : 0
});

// Tabs

(function() {

    $('.tabs_nav > li > a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("data-target"));
        var box = $(this).closest('.tabs');

        $(this).closest('.tabs_nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        box.find('> .tabs_item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());