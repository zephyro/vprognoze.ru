<!doctype html>
<html lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>
        <div class="o-wrapper" id="o-wrapper">
        <div class="wrapper">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->
            <link rel="stylesheet" href="css/font-awesome.min.css">
            <section class="main clearfix">

                <!-- Sidebar Left -->
                <?php include('inc/sidebar_left.inc.php') ?>
                <!-- -->


                <div class="right_block_vp"><!-- header banner fonbet -->

                    <!-- SIDEBAR Right -->
                    <?php include('inc/sidebar_right.inc.php') ?>
                    <!-- -->

                    <article>

                        <div class="page">

                            <div class="news_boxing">
                                <div class="speedbar">
                                        <span id="dle-speedbar">
                                            <a href="#">Впрогнозе</a>
                                            »
                                            <a href="#">Прогнозы</a>
                                            »
                                            <a href="#">Ставки от профессионалов</a>
                                            »
                                            Прогнозы на хоккей
                                        </span>
                                </div>
                            </div>

                            <div class="page__box">

                                <div class="event">
                                    <div class="event__heading event__heading_blue">Рубин - Динамо Москва смотреть прямую трансляцию 2018-11-08 в 19:30</div>
                                    <div class="event__heading event__heading_gray">
                                        <span class="event__heading_elem"><span class="text_uppercase">Россия:</span> Премьер-лига</span>
                                        <span class="event__heading_elem">Начало: 2018-11-08 в 19:30</span>
                                    </div>

                                    <div class="event__wrap">

                                        <ul class="event__meta">
                                            <li>
                                                <a href="#">
                                                    <img src="images/game_meta_icon_02.png" alt="">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img src="images/game_meta_icon_01.png" alt="">
                                                </a>
                                            </li>
                                        </ul>

                                        <div class="event__date"><span class="text_red text_uppercase">live</span></div>

                                        <div class="event__info">

                                            <div class="event__info_player event__info_left">
                                                <div class="event__info_player__logo">
                                                    <img src="images/rubun.png" class="img-fluid" alt="">
                                                </div>
                                                <div class="event__info_player__name"><span>Сент-Луис Блюс</span></div>
                                            </div>

                                            <div class="event__info_data">
                                                <div class="event__info_data__result">
                                                    <span>200</span>
                                                    <span>0</span>
                                                </div>
                                                <div class="event__info_data__stat">2:0 (6:3,6:4)</div>
                                            </div>

                                            <div class="event__info_player event__info_right">
                                                <div class="event__info_player__logo">
                                                    <img src="images/dinamo.png" class="img-fluid" alt="">
                                                </div>
                                                <div class="event__info_player__name"><span>Эдмонтон Ойлерз</span></div>
                                            </div>

                                        </div>

                                        <div class="game__analytics tabs">

                                            <ul class="game__analytics_nav tabs_nav">
                                                <li class="active"><a href="#" data-target=".item1">Прогноз матча</a></li>
                                                <li><a href="#" data-target=".item2">Последние игры</a></li>
                                                <li><a href="#" data-target=".item3">Анализ матча</a></li>
                                            </ul>

                                            <div class="tabs_item item1 active">

                                                <div class="predict">


                                                    <ul class="predict_info">
                                                        <li>
                                                            <span class="predict_info_text">Прогноз:</span>
                                                            <span class="predict_info_value">П1</span>
                                                        </li>
                                                        <li>
                                                            <span class="predict_info_text">Коэффициент:</span>
                                                            <span class="predict_info_value">1.91</span>
                                                        </li>
                                                        <li>
                                                            <span class="predict_info_text">Сумма ставки:</span>
                                                            <span class="predict_info_value">175</span>
                                                        </li>
                                                    </ul>

                                                    <ul class="predict_info_v2 hide">
                                                        <li>
                                                            <span class="predict_info_text">Прогноз:</span>
                                                            <span class="predict_info_value">П1</span>
                                                        </li>
                                                        <li>
                                                            <span class="predict_info_text">Коэффициент:</span>
                                                            <span class="predict_info_value">1.91</span>
                                                        </li>
                                                        <li>
                                                            <span class="predict_info_text">Сумма ставки:</span>
                                                            <span class="predict_info_value">175</span>
                                                        </li>
                                                    </ul>

                                                    <p>Народ! Я уже дал прогноз на победу Ника с форой -2.5, теперь ещё хочу дать чистую победу Австралийца! Кф очень проседает, видел 1.8 на Ника, но пока смотрел футбол и занимался своими делами, то кф просел до 1.49, очень обидно, особенно, когда хотел поставить реальные деньги. Теперь вот вместо 1.8+ буду радоваться малому (1.49). Ну ничего страшного, мы же тут с вами не гонимся за кф! В предыдущем прогнозе я уже писал, что у Маррея нету шансов попросту потому что это первый его матч после травмы и практики нету вовсе. А Кирьос уже прибился к траве на турнире в Штуттгарте, где на этой неделе дошёл до полуфинала и проиграл только великому Маэстро! </p>
                                                    <br/>
                                                    <div class="table-responsive">
                                                        <table class="table predict_table">
                                                            <tr>
                                                                <th>Букмекер</th>
                                                                <th>П1</th>
                                                                <th>Ничья</th>
                                                                <th>П2</th>
                                                                <th>1Х</th>
                                                                <th>12</th>
                                                                <th>Х2</th>
                                                                <th>Тотал</th>
                                                                <th>ТБ</th>
                                                                <th>ТМ</th>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="table_vendor_logo">
                                                                        <img src="images/logo_01.png" class="img-fluid">
                                                                    </div>
                                                                </td>
                                                                <td>1.89</td>
                                                                <td>6.40</td>
                                                                <td>2.32</td>
                                                                <td>1.47</td>
                                                                <td>1.05</td>
                                                                <td>1.72</td>
                                                                <td>7.5</td>
                                                                <td>2.00</td>
                                                                <td>1.74</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="table_vendor_logo">
                                                                        <img src="images/logo_02.png" class="img-fluid">
                                                                    </div>
                                                                </td>
                                                                <td>1.91</td>
                                                                <td>6.60</td>
                                                                <td>2.36</td>
                                                                <td>1.47</td>
                                                                <td>1.05</td>
                                                                <td>1.72</td>
                                                                <td>7.5</td>
                                                                <td>1.78</td>
                                                                <td>1.95</td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                    <div class="post_info">
                                                        <ul>
                                                            <li><i class="fa fa-eye"></i> 25</li>
                                                            <li><a href="#"><span>Поддержка</span></a></li>
                                                            <li><a href="#"><i class="fa fa-comment"></i> <span>Комментарии (0)</span></a></li>
                                                        </ul>
                                                    </div>

                                                    <div class="predict_meta">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-user"></i> <span>Баскет СТО</span></a></li>
                                                            <li><a href="#"><i class="fa fa-bar-chart"></i> <span>3250 (+170)</span></a></li>
                                                        </ul>
                                                        <ul>
                                                            <li>Подписка</li>
                                                            <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-rss"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-commenting"></i></a></li>
                                                        </ul>
                                                        <ul>
                                                            <li>Поделиться</li>
                                                            <li><a href="#"><i class="fa fa-vk"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                        </ul>

                                                    </div>
                                                </div>

                                            </div>

                                            <div class="tabs_item item2">
                                                <div class="games tabs">
                                                    <ul class="games_nav tabs_nav">
                                                        <li class="active"><a href="#" data-target=".item1"><span>Последние </span>игры дома</a></li>
                                                        <li><a href="#" data-target=".item2"><span>Последние </span>игры в гостях</a></li>
                                                    </ul>
                                                    <div class="tabs_item item1 active">
                                                        <div class="table-responsive">
                                                            <table class="table text-center">
                                                                <tr>
                                                                    <th colspan="6">Россия</th>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_defeat">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_draw">Н</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_win">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_defeat">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_draw">Н</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_win">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_defeat">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_draw">Н</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_win">П</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>

                                                        <div class="table-responsive">
                                                            <table class="table text-center">
                                                                <tr>
                                                                    <th colspan="6">Англия</th>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_defeat">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_draw">Н</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_win">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_defeat">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_draw">Н</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_win">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_defeat">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_draw">Н</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_win">П</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tabs_item item2">
                                                        <div class="table-responsive">
                                                            <table class="table text-center">
                                                                <tr>
                                                                    <th colspan="6">Англия</th>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_defeat">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_draw">Н</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_win">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_defeat">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_draw">Н</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_win">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_defeat">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_draw">Н</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Англия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_win">П</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>

                                                        <div class="table-responsive">
                                                            <table class="table text-center">
                                                                <tr>
                                                                    <th colspan="6">Россия</th>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_defeat">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_draw">Н</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_win">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_defeat">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_draw">Н</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_win">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_defeat">П</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_draw">Н</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2018-07-07</td>
                                                                    <td><a href="#">Wor</a></td>
                                                                    <td><a href="#">Россия</a></td>
                                                                    <td><a href="#">Хорватия</a></td>
                                                                    <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                    <td><span class="game_result result_win">П</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tabs_item item3">

                                                <table class="analytics_table mb30">
                                                    <tr>
                                                        <td>
                                                            <div class="analytics_value">49</div>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_gray" style="width: 49%;"></span></div>
                                                        </td>
                                                        <td>Владение</td>
                                                        <td>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 51%;"></span></div>
                                                            <div class="analytics_value">51</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="analytics_value">11</div>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_gray" style="width: 46%;"></span></div>
                                                        </td>
                                                        <td>Удары</td>
                                                        <td>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 54%;"></span></div>
                                                            <div class="analytics_value">13</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="analytics_value">3</div>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 75%;"></span></div>
                                                        </td>
                                                        <td>Удары в створ</td>
                                                        <td>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_gray" style="width: 25%;"></span></div>
                                                            <div class="analytics_value">1</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="analytics_value">8</div>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_gray" style="width: 40%;"></span></div>
                                                        </td>
                                                        <td>Удары мимо</td>
                                                        <td>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 60%;"></span></div>
                                                            <div class="analytics_value">12</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="analytics_value">7</div>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 64%;"></span></div>
                                                        </td>
                                                        <td>Угловые</td>
                                                        <td>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_gray" style="width: 36%;"></span></div>
                                                            <div class="analytics_value">4</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="analytics_value">0</div>
                                                            <div class="analytics_line"><span class="analytics_line_value"></span></div>
                                                        </td>
                                                        <td>Оффсайды</td>
                                                        <td>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 100%;"></span></div>
                                                            <div class="analytics_value">0</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="analytics_value">11</div>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 52%;"></span></div>
                                                        </td>
                                                        <td>Фолы</td>
                                                        <td>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_gray" style="width: 48%;"></span></div>
                                                            <div class="analytics_value">10</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="analytics_value">1</div>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 50%;"></span></div>
                                                        </td>
                                                        <td>Желтые карточки</td>
                                                        <td>
                                                            <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 50%;"></span></div>
                                                            <div class="analytics_value">1</div>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <div class="analytics_graph mb30">
                                                    <canvas id="graph01"></canvas>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </article>

                </div>

                <div class="clr"></div>
                <div id="dialog-form"></div>

            </section>
            <!--noindex-->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>
    </div>

    <!-- upd 01.05.2018 -->

        <!-- Modal  -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts  -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script>
            var config = {
                type: 'line',
                data: {
                    labels: ["0", "7 дней", "14 дней", "30 дней"],
                    datasets: [{
                        label: '',
                        data: [0, 20, 60, 70, 90, 100],
                        backgroundColor: [
                            'rgba(255, 255, 255, 0)'
                        ],
                        borderColor: [
                            'rgba(88,93,209,1)'
                        ],
                        borderWidth: 2
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Победы'
                    },
                    legend: {
                        display: false,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: false
                    },
                    scales: {
                        xAxes: [{
                            display: false,
                            scaleLabel: {
                                display: false,
                                labelString: ''
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: false,
                                labelString: ''
                            },
                            ticks: {
                                min: 0,
                                max: 120,
                                stepSize: 30
                            }
                        }]
                    }
                }
            };

            window.onload = function() {
                var ctx = document.getElementById('graph01').getContext('2d');
                window.myLine = new Chart(ctx, config);
            };
        </script>

    </body>
</html>
