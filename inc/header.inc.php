<HEADER>
    <DIV class="level_1">
        <SECTION>
            <DIV class="level_1_layout"><A class="c-button" id="c-button--slide-left"><SPAN class="bar"></SPAN><SPAN class="bar"></SPAN><SPAN class="bar"></SPAN></A><A
                        class="header-logo" href="https://vprognoze.ru/"><IMG class="logo_mobile" alt="" src="files/logo_mobile.png"><IMG class="logo_mobile_large" alt="" src="files/logo_vp.png"></A>
                <DIV class="mobiheader">
                    <STYLE scoped="">
                        .dropdown {
                            display: none
                        }

                        .dropdown.show {
                            display: block;
                        }

                        .opera_login input[type="submit"] {
                            display: block !important;
                            width: 196px !important;
                        }

                        .opera_login input {
                            width: 184px !important;
                            display: block !important;
                        }
                    </STYLE>
                    <!--noindex-->
                    <DIV class="time">
                        <FORM id="frm_timezone" action="/" method="post"><A class="link_time" id="dropdown"
                                                                            onclick="dropdowntime(); return false;"><I
                                        class="time_ico"></I><SPAN>00:08 -
    02.05.2018 (+3)</SPAN><I class="arrow"></I></A>
                            <DIV class="dropdown"><INPUT id="get_gt" type="hidden"> <INPUT name="timezone"
                                                                                           id="timezone"
                                                                                           type="hidden">

                                <UL>
                                    <LI><A onclick="changeTimeOffset(-12);">09:08 - 01.05.2018 (-12)</A></LI>
                                    <LI><A onclick="changeTimeOffset(-11);">10:08 - 01.05.2018 (-11)</A></LI>
                                    <LI><A onclick="changeTimeOffset(-10);">11:08 - 01.05.2018 (-10)</A></LI>
                                    <LI><A onclick="changeTimeOffset(-9);">12:08 - 01.05.2018 (-9)</A></LI>
                                    <LI><A onclick="changeTimeOffset(-8);">13:08 - 01.05.2018 (-8)</A></LI>
                                    <LI><A onclick="changeTimeOffset(-7);">14:08 - 01.05.2018 (-7)</A></LI>
                                    <LI><A onclick="changeTimeOffset(-6);">15:08 - 01.05.2018 (-6)</A></LI>
                                    <LI><A onclick="changeTimeOffset(-5);">16:08 - 01.05.2018 (-5)</A></LI>
                                    <LI><A onclick="changeTimeOffset(-4);">17:08 - 01.05.2018 (-4)</A></LI>
                                    <LI><A onclick="changeTimeOffset(-3);">18:08 - 01.05.2018 (-3)</A></LI>
                                    <LI><A onclick="changeTimeOffset(-2);">19:08 - 01.05.2018 (-2)</A></LI>
                                    <LI><A onclick="changeTimeOffset(-1);">20:08 - 01.05.2018 (-1)</A></LI>
                                    <LI><A onclick="changeTimeOffset(0);">21:08 - 01.05.2018 (+0)</A></LI>
                                    <LI><A onclick="changeTimeOffset(1);">22:08 - 01.05.2018 (+1)</A></LI>
                                    <LI><A onclick="changeTimeOffset(2);">23:08 - 01.05.2018 (+2)</A></LI>
                                    <LI><A onclick="changeTimeOffset(3);">00:08 - 02.05.2018 (+3)</A></LI>
                                    <LI><A onclick="changeTimeOffset(4);">01:08 - 02.05.2018 (+4)</A></LI>
                                    <LI><A onclick="changeTimeOffset(5);">02:08 - 02.05.2018 (+5)</A></LI>
                                    <LI><A onclick="changeTimeOffset(6);">03:08 - 02.05.2018 (+6)</A></LI>
                                    <LI><A onclick="changeTimeOffset(7);">04:08 - 02.05.2018 (+7)</A></LI>
                                    <LI><A onclick="changeTimeOffset(8);">05:08 - 02.05.2018 (+8)</A></LI>
                                    <LI><A onclick="changeTimeOffset(9);">06:08 - 02.05.2018 (+9)</A></LI>
                                    <LI><A onclick="changeTimeOffset(10);">07:08 - 02.05.2018 (+10)</A></LI>
                                    <LI><A onclick="changeTimeOffset(11);">08:08 - 02.05.2018 (+11)</A></LI>
                                    <LI><A onclick="changeTimeOffset(12);">09:08 - 02.05.2018 (+12)</A></LI>
                                    <LI><A onclick="changeTimeOffset(13);">10:08 - 02.05.2018
                                            (+13)</A></LI>
                                </UL>
                            </DIV>
                        </FORM>
                    </DIV>
                    <DIV class="other_login"><SPAN>Войти через</SPAN>
                        <DIV class="ulogin_form">
                            <DIV data-ulogin="display=small;fields=first_name,last_name,nickname,email,photo;optional=city,country;providers=vkontakte,odnoklassniki,mailru,facebook;hidden=other;redirect_uri=https%3A%2F%2Fvprognoze.ru%2F%3Fdo%3Dulogin%26backurl%3D%25252F%25253Fdo%25253Dshop%252526action%25253Drating%252526cid%25253D201804%252526uid%25253D645435%252526utm_referrer%25253D%252526fa821dba_ipp_key%25253D1525208815913%2525252flFKhQh9VojfJdTke8uiEUw%2525253d%2525253d%252526fa821dba_ipp_uid1%25253D1525207135519%252526fa821dba_ipp_uid2%25253DAuwLD354PY3Ftxhf%2525252f8Baql8fHg7IZp9u9Sfuc8w%2525253d%2525253d"></DIV>
                        </DIV>
                    </DIV>
                    <DIV class="login">
                        <BUTTON class="profile_button"
                                onclick="$('.profile_block').slideToggle('fast', function() {$('.profile_button').toggleClass('active', $(this).is(':visible'));}); return false;"></BUTTON>

                        <DIV class="profile_block"><A
                                    href="https://vprognoze.ru/?do=register">Регистрация</A> <A class="light"
                                                                                                onclick="$('#login_box').slideToggle('fast', function() {$('.light').toggleClass('active', $(this).is(':visible'));}); return false;"
                                                                                                href="https://vprognoze.ru/?do=shop&amp;action=rating&amp;cid=201804&amp;uid=645435&amp;utm_referrer=&amp;fa821dba_ipp_key=1525208815913%2flFKhQh9VojfJdTke8uiEUw%3d%3d&amp;fa821dba_ipp_uid1=1525207135519&amp;fa821dba_ipp_uid2=AuwLD354PY3Ftxhf%2f8Baql8fHg7IZp9u9Sfuc8w%3d%3d#">Войти</A>
                        </DIV>
                    </DIV>
                    <DIV id="login_box">
                        <FORM action="/" method="post"><INPUT name="login_name" type="text" placeholder="Логин"
                                                              value=""><INPUT name="login_password"
                                                                              type="password"
                                                                              placeholder="Пароль"
                                                                              value=""><INPUT
                                    name="login_user_token" type="hidden" value="3618457b022ff7310e3ee84e7e3eb931">
                            <INPUT type="submit" value="Вход"><INPUT name="login" id="login" type="hidden"
                                                                     value="submit">

                            <DIV class="lost_password"><A href="https://vprognoze.ru/index.php?do=lostpassword"
                                                          rel="nofollow">Забыли пароль?</A></DIV>
                            <DIV class="center">
                                <DIV class="uLogin" id="uLogin2"
                                     data-ulogin="display=small;fields=first_name,last_name,nickname,email,photo;optional=city,country;providers=vkontakte,odnoklassniki,mailru,facebook;hidden=other;redirect_uri=https%3A%2F%2Fvprognoze.ru%2F%3Fdo%3Dulogin%26backurl%3D%25252F">
                                    <A
                                            title="Вконтакте" class="vk"><IMG alt="Вконтакте"
                                                                              src="files/spacer.gif"
                                                                              data-uloginbutton="vkontakte"></A>
                                    <A title="Одноклассники"
                                       class="ok"><IMG alt="Одноклассники"
                                                       src="files/spacer.gif"
                                                       data-uloginbutton="odnoklassniki"></A> <A title="Yandex"
                                                                                                 class="yandex"><IMG
                                                alt="Yandex"
                                                src="files/spacer.gif"
                                                data-uloginbutton="yandex"></A> <A title="Mail.RU" class="mail"><IMG
                                                alt="Mail.RU"
                                                src="files/spacer.gif"
                                                data-uloginbutton="mailru"></A></DIV>
                            </DIV>
                        </FORM>
                    </DIV>
                    <!--/noindex--> </DIV>
                <A class="up_link" id="up" onclick="return up()"
                   href="https://vprognoze.ru/?do=shop&amp;action=rating&amp;cid=201804&amp;uid=645435&amp;utm_referrer=&amp;fa821dba_ipp_key=1525208815913%2flFKhQh9VojfJdTke8uiEUw%3d%3d&amp;fa821dba_ipp_uid1=1525207135519&amp;fa821dba_ipp_uid2=AuwLD354PY3Ftxhf%2f8Baql8fHg7IZp9u9Sfuc8w%3d%3d#">Вверх<SPAN></SPAN></A>
            </DIV>
            <DIV class="light" id="login_box">
                <UL></UL>
            </DIV>
        </SECTION>
    </DIV>
    <DIV class="level_2">
        <SECTION><A class="header-logo" href="https://vprognoze.ru/"><IMG
                        class="logo_big" alt=""
                        src="files/logo_vp.png"></A>
            <DIV class="desktop_gorizont_menu">
                <MENU><!--noindex-->
                    <LI><A href="https://vprognoze.ru/turnir/">Турниры</A></LI>
                    <LI><A href="https://vprognoze.ru/topforecast/">Топ ставки</A></LI>
                    <LI><A href="https://vprognoze.ru/chart/">Анализ игр</A></LI>
                    <LI class="vilki_bl"><A href="https://vprognoze.ru/vilki/">Вилки PRO</A>
                        <UL class="subitem_menu">
                            <LI><A href="https://vprognoze.ru/livevilki/">Вилки LIVE</A></LI>
                        </UL>
                    </LI><!--/noindex-->

                    <LI><A title="прямые трансляции"
                           href="https://vprognoze.ru/onlinevideo/">Трансляции</A></LI>
                    <LI class="big "><A href="https://vprognoze.ru/resultat/">Результаты
                            LIVE</A></LI>
                    <LI><A href="https://vprognoze.ru/forum/">Форум</A></LI>
                </MENU>
                <DIV class="sb-icon-search"><B></B></DIV>
            </DIV>
            <DIV class="mobile_gorizont_menu">
                <LINK href="files/index(6).css" rel="stylesheet">

                <MENU class="owl-carousel owl-menu-main" id="owl-demo"><!--noindex-->

                    <LI class="item link"><A href="https://vprognoze.ru/turnir/">Турниры</A></LI>
                    <LI class="item link"><A href="https://vprognoze.ru/topforecast/">Топ
                            ставки</A></LI>
                    <LI class="item link"><A href="https://vprognoze.ru/chart/">Анализ
                            игр</A></LI>
                    <LI class="item link"><A href="https://vprognoze.ru/vilki/">Вилки PRO</A></LI>
                    <LI class="item link link-livevilki"><A
                                href="https://vprognoze.ru/livevilki/">Вилки LIVE</A></LI><!--/noindex-->

                    <LI class="item link"><A
                                href="https://vprognoze.ru/onlinevideo/">Трансляции</A></LI>
                    <LI class="item link"><A href="https://vprognoze.ru/resultat/">Результаты
                            LIVE</A></LI>
                    <LI class="item link"><A href="https://vprognoze.ru/forum/">Форум</A></LI>
                    <LI class="item link">
                        <DIV class="sb-icon-search"><B></B></DIV>
                    </LI>
                </MENU>

            </DIV>
            <DIV class="overlay"></DIV>
            <DIV class="block_open_search" style="display: none;"><SPAN class="sb-icon_close">Х</SPAN>
                <DIV>Введите запрос для поиска матча:</DIV>
                <DIV class="tabs">
                    <UL class="tabNavigation">
                        <LI>
                            <A href="https://vprognoze.ru/?do=shop&amp;action=rating&amp;cid=201804&amp;uid=645435&amp;utm_referrer=&amp;fa821dba_ipp_key=1525208815913%2flFKhQh9VojfJdTke8uiEUw%3d%3d&amp;fa821dba_ipp_uid1=1525207135519&amp;fa821dba_ipp_uid2=AuwLD354PY3Ftxhf%2f8Baql8fHg7IZp9u9Sfuc8w%3d%3d#first">Яндекс</A>
                        </LI>

                        <LI>
                            <A href="https://vprognoze.ru/?do=shop&amp;action=rating&amp;cid=201804&amp;uid=645435&amp;utm_referrer=&amp;fa821dba_ipp_key=1525208815913%2flFKhQh9VojfJdTke8uiEUw%3d%3d&amp;fa821dba_ipp_uid1=1525207135519&amp;fa821dba_ipp_uid2=AuwLD354PY3Ftxhf%2f8Baql8fHg7IZp9u9Sfuc8w%3d%3d#second">Google</A>
                        </LI>

                        <LI>
                            <A href="https://vprognoze.ru/?do=shop&amp;action=rating&amp;cid=201804&amp;uid=645435&amp;utm_referrer=&amp;fa821dba_ipp_key=1525208815913%2flFKhQh9VojfJdTke8uiEUw%3d%3d&amp;fa821dba_ipp_uid1=1525207135519&amp;fa821dba_ipp_uid2=AuwLD354PY3Ftxhf%2f8Baql8fHg7IZp9u9Sfuc8w%3d%3d#third">Сайт</A>
                        </LI>
                    </UL>
                    <DIV class="search_pad" id="first" style="display: none;">
                        <DIV class="clearfix">
                            <DIV class="ya-site-form ya-site-form_inited_no"
                                 onclick="return {'action':'//vprognoze.ru/?do=search&amp;ya=1','arrow':false,'bg':'transparent','fontsize':12,'fg':'#000000','language':'ru','logo':'rb','publicname':'ќќќќќ ќќќќќќќќќ','suggest':true,'target':'_self','tld':'ru','type':3,'usebigdictionary':true,'searchid':2240058,'input_fg':'#000000','input_bg':'#ffffff','input_fontStyle':'normal','input_fontWeight':'normal','input_placeholder':null,'input_placeholderColor':'#000000','input_borderColor':'#7f9db9'}">
                                <FORM action="https://yandex.ru/search/site/" method="get"
                                      target="_self"><INPUT name="searchid" type="hidden" value="2240058"><INPUT
                                            name="l10n" type="hidden" value="ru"><INPUT name="reqenc" type="hidden"><INPUT
                                            name="text" type="search"><INPUT type="submit" value="ќќќќќ"></FORM>
                            </DIV>
                            <STYLE type="text/css">.ya-page_js_yes .ya-site-form_inited_no {
                                    display: none;
                                }</STYLE>

                        </DIV>
                    </DIV>
                    <DIV class="search_pad" id="second">

                    </DIV>
                    <DIV class="search_pad" id="third">
                        <DIV class="clearfix">
                            <FORM name="searchform" action="/" method="post"><INPUT name="story"
                                                                                    class="vp-search-input"
                                                                                    autofocus="autofocus"
                                                                                    type="text"
                                                                                    placeholder="Поиск по сайту..."
                                                                                    value="">
                                <INPUT class="vp-search-submit" type="submit"><INPUT name="do" type="hidden"
                                                                                     value="search">
                                <INPUT name="subaction" type="hidden" value="search">
                            </FORM>
                        </DIV>
                    </DIV>
                </DIV>
            </DIV>
        </SECTION>
    </DIV>
</HEADER>