<div class="hide">
    <div class="myModal" id="order">
        <div class="myModal__header">
            <div class="myModal__title">Пакет подписки на выбранного вами прогнозиста - Бен.Брунович</div>
            <div class="myModal__text">Выберите необходимый период и оплатите доступ:</div>
        </div>
        <div class="myModal__content">
            <form class="form">
                <div class="myModal__form">
                    <div class="form-group">
                        <label class="form-radio">
                            <input type="radio" name="period" value="" checked>
                            <span class="form-radio-text"><strong>1 день</strong> доступа за <strong class="text-blue">500 руб.</strong></span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-radio">
                            <input type="radio" name="period" value="">
                            <span class="form-radio-text"><strong>3 день</strong> доступа за <strong class="text-blue">1000 руб.</strong></span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-radio">
                            <input type="radio" name="period" value="">
                            <span class="form-radio-text"><strong>7 день</strong> доступа за <strong class="text-blue">2000 руб.</strong></span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-radio">
                            <input type="radio" name="period" value="">
                            <span class="form-radio-text"><strong>14 день</strong> доступа за <strong class="text-blue">3000 руб.</strong></span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-radio">
                            <input type="radio" name="period" value="">
                            <span class="form-radio-text"><strong>21 день</strong> доступа за <strong class="text-blue">4000 руб.</strong></span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-radio">
                            <input type="radio" name="period" value="">
                            <span class="form-radio-text"><strong>30 день</strong> доступа за <strong class="text-blue">5000 руб.</strong></span>
                        </label>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-blue btn-md btn-send">оплатить</button>
                </div>
            </form>
        </div>
        <div class="myModal__footer">
            <ul>
                <li>- Прогнозы рассылаются на почту, в ЛС, СМС (если телефон указан в профиле)</li>
                <li>- Подписка продлевается на 1 день, если не было дано ни одного прогноза</li>
                <li>- Eсли игровой день был в минусе, то на каждый минусовой прогноз выдается купон(для подписок без гарантии)</li>
                <li>- С гарантией возврата - если прибыль прогнозиста отрицательная по прогнозам из интервала Вашей подписки - Мы возвращаем Вам 85% стоимости подписки</li>
            </ul>
        </div>
    </div>
</div>