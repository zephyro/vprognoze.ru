<!doctype html>
<html lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>
        <div class="o-wrapper" id="o-wrapper">
        <div class="wrapper">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->
            <link rel="stylesheet" href="css/font-awesome.min.css">
            <section class="main clearfix">

                <!-- Sidebar Left -->
                <?php include('inc/sidebar_left.inc.php') ?>
                <!-- -->


                <div class="right_block_vp"><!-- header banner fonbet -->

                    <!-- SIDEBAR Right -->
                    <?php include('inc/sidebar_right.inc.php') ?>
                    <!-- -->

                    <article>

                        <div class="page">

                            <div class="news_boxing">
                                <div class="speedbar">
                                        <span id="dle-speedbar">
                                            <a href="#">Впрогнозе</a>
                                            »
                                            <a href="#">Прогнозы</a>
                                            »
                                            <a href="#">Ставки от профессионалов</a>
                                            »
                                            Прогнозы на хоккей
                                        </span>
                                </div>
                            </div>

                            <div class="page__box">

                                <div class="game">

                                    <div class="game__heading">
                                        <div class="game__heading_name">
                                            <h1>Теннис. Мировой тур ATP. Категория 250. Штутгарт, Германия. Одиночный разряд.</h1>
                                            <span>1/2 финала</span>
                                        </div>
                                        <div class="game_heading_date">12.03.2015</div>
                                    </div>
                                    <ul class="game__meta">
                                        <li>
                                            <a href="#">
                                                <img src="images/game_meta_icon_02.png" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="images/game_meta_icon_01.png" alt="">
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="game__info">
                                        <div class="game__info_player block_right">
                                            <div class="game__info_player_wrap">
                                                <div class="game__info_player_flag">
                                                    <img src="images/flag_01.png" class="img-fluid" alt="">
                                                </div>
                                                <div class="game__info_player_name">Россия</div>
                                            </div>
                                        </div>
                                        <div class="game__info_data">
                                            <div class="game__info_data_time">74 мин</div>
                                            <div class="game__info_data_result">
                                                <span>1</span>
                                                <span>1</span>
                                            </div>
                                            <ul class="game__info_data_link">
                                                <li>
                                                    <a href="#">
                                                        <img src="img/game_info_graph_icon_01.png" alt="">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="img/game_info_graph_icon_02.png" alt="">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="game__info_player block_left">
                                            <div class="game__info_player_wrap">
                                                <div class="game__info_player_flag">
                                                    <img src="images/flag_02.png" class="img-fluid" alt="">
                                                </div>
                                                <div class="game__info_player_name">Египет</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="game__stats">

                                        <div class="game__stats_row row_left">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">47'</span>
                                                <span class="game__stats_icon">
                                                    <img src="img/icon_football.png" alt="">
                                                </span>
                                                <span class="game__stats_name">A. Fathy (o.g.)</span>
                                            </div>
                                        </div>


                                        <div class="game__stats_row row_right">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">57’</span>
                                                <span class="game__stats_icon">
                                                        <img src="img/icon_soccer_faults_cards.png" alt="">
                                                    </span>
                                                <span class="game__stats_name">Trezeguet</span>

                                            </div>
                                        </div>


                                        <div class="game__stats_row row_left">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">59'</span>
                                                <span class="game__stats_icon">
                                                    <img src="img/icon_football.png" alt="">
                                                </span>
                                                <span class="game__stats_name">D. Cheryshev</span>
                                            </div>
                                        </div>
                                        <div class="game__stats_row row_left">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">62'</span>
                                                <span class="game__stats_icon">
                                                    <img src="img/icon_football.png" alt="">
                                                </span>
                                                <span class="game__stats_name">A. Dzyuba</span>
                                            </div>
                                        </div>


                                        <div class="game__stats_row row_right">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">64'</span>
                                                <span class="game__stats_icon">
                                                        <img src="img/icon_exchange.png" alt="">
                                                    </span>
                                                <span class="game__stats_name">A. Warda</span>
                                            </div>
                                        </div>
                                        <div class="game__stats_row row_right">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">64'</span>
                                                <span class="game__stats_icon">
                                                        <img src="img/icon_exchange.png" alt="">
                                                    </span>
                                                <a class="game__stats_name" href="#">Amr Warda / Mohamed Elneny</a>
                                            </div>
                                        </div>
                                        <div class="game__stats_row row_right">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">68'</span>
                                                <span class="game__stats_icon">
                                                        <img src="img/icon_exchange.png" alt="">
                                                    </span>
                                                <span class="game__stats_name">R. Sobhi</span>
                                            </div>
                                        </div>
                                        <div class="game__stats_row row_right">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">68'</span>
                                                <span class="game__stats_icon">
                                                        <img src="img/icon_exchange.png" alt="">
                                                    </span>
                                                <a class="game__stats_name" href="#">Ramadan Sobhi / Trezeguet</a>
                                            </div>
                                        </div>
                                        <div class="game__stats_row row_right">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">73'</span>
                                                <span class="game__stats_icon">
                                                        <img src="img/icon_football.png" alt="">
                                                    </span>
                                                <span class="game__stats_name">M. Salah (pen.)</span>
                                            </div>
                                        </div>


                                        <div class="game__stats_row row_left">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">74'</span>
                                                <span class="game__stats_icon">
                                                    <img src="img/icon_exchange.png" alt="">
                                                </span>
                                                <a class="game__stats_name" href="#">Daler Kuziaev / Denis Cheryshev</a>
                                            </div>
                                        </div>
                                        <div class="game__stats_row row_left">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">74'</span>
                                                <span class="game__stats_icon">
                                                    <img src="img/icon_exchange.png" alt="">
                                                </span>
                                                <span class="game__stats_name">D. Kuzyayev</span>
                                            </div>
                                        </div>
                                        <div class="game__stats_row row_left">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">79'</span>
                                                <span class="game__stats_icon">
                                                    <img src="img/icon_exchange.png" alt="">
                                                </span>
                                                <span class="game__stats_name">F. Smolov</span>
                                            </div>
                                        </div>
                                        <div class="game__stats_row row_left">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">79'</span>
                                                <span class="game__stats_icon">
                                                    <img src="img/icon_exchange.png" alt="">
                                                </span>
                                                <a class="game__stats_name" href="#">Fedor Smolov / Artem Dzyuba</a>
                                            </div>
                                        </div>


                                        <div class="game__stats_row row_right">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">82'</span>
                                                <span class="game__stats_icon">
                                                        <img src="img/icon_exchange.png" alt="">
                                                    </span>
                                                <span>Kahraba</span>
                                            </div>
                                        </div>
                                        <div class="game__stats_row row_right">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">82'</span>
                                                <span class="game__stats_icon">
                                                        <img src="img/icon_football.png" alt="">
                                                    </span>
                                                <a href="#">Kahraba / Marwan Mohsen</a>
                                            </div>
                                        </div>


                                        <div class="game__stats_row row_left">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">84’</span>
                                                <span class="game__stats_icon">
                                                    <img src="img/icon_soccer_faults_cards.png" alt="">
                                                </span>
                                                <span class="game__stats_name">F. Smolov</span>
                                            </div>
                                        </div>
                                        <div class="game__stats_row row_left">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">86'</span>
                                                <span class="game__stats_icon">
                                                    <img src="img/icon_exchange.png" alt="">
                                                </span>
                                                <span class="game__stats_name">F. Kudryashov</span>
                                            </div>
                                        </div>
                                        <div class="game__stats_row row_left">
                                            <div class="game__stats_elem">
                                                <span class="game__stats_time">86'</span>
                                                <span class="game__stats_icon">
                                                    <img src="img/icon_exchange.png" alt="">
                                                </span>
                                                <a class="game__stats_name" href="#">Fedor Kudryashov / Yury Zhirkov</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="game__analytics tabs">

                                        <ul class="game__analytics_nav tabs_nav">
                                            <li class="active"><a href="#" data-target=".item1">Прогноз матча</a></li>
                                            <li><a href="#" data-target=".item2">Последние игры</a></li>
                                            <li><a href="#" data-target=".item3">Анализ матча</a></li>
                                        </ul>

                                        <div class="tabs_item item1 active">

                                            <div class="predict">


                                                <ul class="predict_info">
                                                    <li>
                                                        <span class="predict_info_text">Прогноз:</span>
                                                        <span class="predict_info_value">П1</span>
                                                    </li>
                                                    <li>
                                                        <span class="predict_info_text">Коэффициент:</span>
                                                        <span class="predict_info_value">1.91</span>
                                                    </li>
                                                    <li>
                                                        <span class="predict_info_text">Сумма ставки:</span>
                                                        <span class="predict_info_value">175</span>
                                                    </li>
                                                </ul>

                                                <ul class="predict_data">
                                                    <li>Начало игры: <i>(2018-07-10 21:00)</i></li>
                                                    <li>Результат: 1:0 (0:0)</li>
                                                </ul>

                                                <ul class="predict_info_v2 hide">
                                                    <li>
                                                        <span class="predict_info_text">Прогноз:</span>
                                                        <span class="predict_info_value">П1</span>
                                                    </li>
                                                    <li>
                                                        <span class="predict_info_text">Коэффициент:</span>
                                                        <span class="predict_info_value">1.91</span>
                                                    </li>
                                                    <li>
                                                        <span class="predict_info_text">Сумма ставки:</span>
                                                        <span class="predict_info_value">175</span>
                                                    </li>
                                                </ul>

                                                <p>Народ! Я уже дал прогноз на победу Ника с форой -2.5, теперь ещё хочу дать чистую победу Австралийца! Кф очень проседает, видел 1.8 на Ника, но пока смотрел футбол и занимался своими делами, то кф просел до 1.49, очень обидно, особенно, когда хотел поставить реальные деньги. Теперь вот вместо 1.8+ буду радоваться малому (1.49). Ну ничего страшного, мы же тут с вами не гонимся за кф! В предыдущем прогнозе я уже писал, что у Маррея нету шансов попросту потому что это первый его матч после травмы и практики нету вовсе. А Кирьос уже прибился к траве на турнире в Штуттгарте, где на этой неделе дошёл до полуфинала и проиграл только великому Маэстро! </p>
                                                <br/>
                                                <div class="table-responsive">
                                                    <table class="table predict_table">
                                                        <tr>
                                                            <th>Букмекер</th>
                                                            <th>П1</th>
                                                            <th>Ничья</th>
                                                            <th>П2</th>
                                                            <th>1Х</th>
                                                            <th>12</th>
                                                            <th>Х2</th>
                                                            <th>Тотал</th>
                                                            <th>ТБ</th>
                                                            <th>ТМ</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="table_vendor_logo">
                                                                    <img src="images/logo_01.png" class="img-fluid">
                                                                </div>
                                                            </td>
                                                            <td>1.89</td>
                                                            <td>6.40</td>
                                                            <td>2.32</td>
                                                            <td>1.47</td>
                                                            <td>1.05</td>
                                                            <td>1.72</td>
                                                            <td>7.5</td>
                                                            <td>2.00</td>
                                                            <td>1.74</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="table_vendor_logo">
                                                                    <img src="images/logo_02.png" class="img-fluid">
                                                                </div>
                                                            </td>
                                                            <td>1.91</td>
                                                            <td>6.60</td>
                                                            <td>2.36</td>
                                                            <td>1.47</td>
                                                            <td>1.05</td>
                                                            <td>1.72</td>
                                                            <td>7.5</td>
                                                            <td>1.78</td>
                                                            <td>1.95</td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div class="post_info">
                                                    <ul>
                                                        <li><i class="fa fa-eye"></i> 25</li>
                                                        <li><a href="#"><span>Поддержка</span></a></li>
                                                        <li><a href="#"><i class="fa fa-comment"></i> <span>Комментарии (0)</span></a></li>
                                                    </ul>
                                                </div>

                                                <div class="predict_meta">
                                                    <ul>
                                                        <li><a href="#"><i class="fa fa-user"></i> <span>Баскет СТО</span></a></li>
                                                        <li><a href="#"><i class="fa fa-bar-chart"></i> <span>3250 (+170)</span></a></li>
                                                    </ul>
                                                    <ul>
                                                        <li>Подписка</li>
                                                        <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-commenting"></i></a></li>
                                                    </ul>
                                                    <ul>
                                                        <li>Поделиться</li>
                                                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                    </ul>

                                                </div>
                                            </div>

                                        </div>

                                        <div class="tabs_item item2">
                                            <div class="games tabs">
                                                <ul class="games_nav tabs_nav">
                                                    <li class="active"><a href="#" data-target=".item1"><span>Последние </span>игры дома</a></li>
                                                    <li><a href="#" data-target=".item2"><span>Последние </span>игры в гостях</a></li>
                                                </ul>
                                                <div class="tabs_item item1 active">
                                                    <div class="table-responsive">
                                                        <table class="table text-center">
                                                            <tr>
                                                                <th colspan="6">Россия</th>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_defeat">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_draw">Н</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_win">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_defeat">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_draw">Н</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_win">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_defeat">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_draw">Н</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_win">П</span></td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                    <div class="table-responsive">
                                                        <table class="table text-center">
                                                            <tr>
                                                                <th colspan="6">Англия</th>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_defeat">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_draw">Н</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_win">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_defeat">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_draw">Н</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_win">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_defeat">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_draw">Н</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_win">П</span></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tabs_item item2">
                                                    <div class="table-responsive">
                                                        <table class="table text-center">
                                                            <tr>
                                                                <th colspan="6">Англия</th>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_defeat">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_draw">Н</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_win">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_defeat">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_draw">Н</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_win">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_defeat">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_draw">Н</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Англия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_win">П</span></td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                    <div class="table-responsive">
                                                        <table class="table text-center">
                                                            <tr>
                                                                <th colspan="6">Россия</th>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_defeat">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_draw">Н</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_win">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_defeat">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_draw">Н</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_win">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_defeat">П</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_draw">Н</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2018-07-07</td>
                                                                <td><a href="#">Wor</a></td>
                                                                <td><a href="#">Россия</a></td>
                                                                <td><a href="#">Хорватия</a></td>
                                                                <td class="text_nowrap"><strong>1 – 1</strong></td>
                                                                <td><span class="game_result result_win">П</span></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tabs_item item3">

                                            <table class="analytics_table mb30">
                                                <tr>
                                                    <td>
                                                        <div class="analytics_value">49</div>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_gray" style="width: 49%;"></span></div>
                                                    </td>
                                                    <td>Владение</td>
                                                    <td>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 51%;"></span></div>
                                                        <div class="analytics_value">51</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="analytics_value">11</div>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_gray" style="width: 46%;"></span></div>
                                                    </td>
                                                    <td>Удары</td>
                                                    <td>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 54%;"></span></div>
                                                        <div class="analytics_value">13</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="analytics_value">3</div>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 75%;"></span></div>
                                                    </td>
                                                    <td>Удары в створ</td>
                                                    <td>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_gray" style="width: 25%;"></span></div>
                                                        <div class="analytics_value">1</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="analytics_value">8</div>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_gray" style="width: 40%;"></span></div>
                                                    </td>
                                                    <td>Удары мимо</td>
                                                    <td>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 60%;"></span></div>
                                                        <div class="analytics_value">12</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="analytics_value">7</div>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 64%;"></span></div>
                                                    </td>
                                                    <td>Угловые</td>
                                                    <td>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_gray" style="width: 36%;"></span></div>
                                                        <div class="analytics_value">4</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="analytics_value">0</div>
                                                        <div class="analytics_line"><span class="analytics_line_value"></span></div>
                                                    </td>
                                                    <td>Оффсайды</td>
                                                    <td>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 100%;"></span></div>
                                                        <div class="analytics_value">0</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="analytics_value">11</div>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 52%;"></span></div>
                                                    </td>
                                                    <td>Фолы</td>
                                                    <td>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_gray" style="width: 48%;"></span></div>
                                                        <div class="analytics_value">10</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="analytics_value">1</div>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 50%;"></span></div>
                                                    </td>
                                                    <td>Желтые карточки</td>
                                                    <td>
                                                        <div class="analytics_line"><span class="analytics_line_value analytics_line_red" style="width: 50%;"></span></div>
                                                        <div class="analytics_value">1</div>
                                                    </td>
                                                </tr>
                                            </table>

                                            <div class="analytics_graph mb30">
                                                <canvas id="graph01"></canvas>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="page__box">
                                <div class="forecast_poll">
                                    <div class="poll_title">Сыграет этот прогноз ?</div>
                                    <div class="poll_legend"><span>Победа Н.Киргиос</span></div>

                                    <div class="poll_row">
                                        <div class="poll_content">
                                            <div class="poll_elem poll_elem_green">
                                                <div class="poll_elem_value">86%</div>
                                                <div class="poll_elem_result">
                                                    <div class="poll_elem_line">
                                                        <div class="poll_elem_line_value" style="width: 80%">
                                                            <span>4</span>
                                                        </div>
                                                    </div>
                                                    <ul class="poll_elem_user">
                                                        <li>
                                                            <a href="#" title="user name">
                                                                <img src="images/user_01.jpg" class="img-fluid" alt="">
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" title="user name">
                                                                <img src="images/user_02.jpg" class="img-fluid" alt="">
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" title="user name">
                                                                <img src="images/user_03.jpg" class="img-fluid" alt="">
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" title="user name">
                                                                <img src="images/user_04.jpg" class="img-fluid" alt="">
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" title="user name">
                                                                <img src="images/user_01.jpg" class="img-fluid" alt="">
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="poll_elem poll_elem_red">
                                                <div class="poll_elem_value">14%</div>
                                                <div class="poll_elem_result">
                                                    <div class="poll_elem_line">
                                                        <div class="poll_elem_line_value" style="width: 20%">
                                                            <span>1</span>
                                                        </div>
                                                    </div>
                                                    <ul class="poll_elem_user">
                                                        <li>
                                                            <a href="#" title="user name">
                                                                <img src="images/user_01.jpg" class="img-fluid" alt="">
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="poll_buttons">
                                            <li>
                                                <button type="submit" class="btn btn-green">Да</button>
                                            </li>
                                            <li>
                                                <button type="submit" class="btn btn-red">Нет</button>
                                            </li>
                                        </ul>

                                    </div>

                                    <div class="poll_number">Всего проголосовало: 5</div>
                                </div>
                            </div>

                            <div class="page__box">
                                <div class="page__box_wrap">
                                    <div class="page__box_title">Похожие прогнозы</div>
                                    <ul class="alike">
                                        <li>
                                            <a href="#">
                                                <span>ФОРА2 по геймам (3) с кф за 1.64</span>
                                            </a>
                                            <span>от verniak01 17.06.2018</span>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>
                                                    ФОРА2 по геймам (3) с кф за 1.64
                                                </span>
                                            </a>
                                            <span>от verniak01 17.06.2018</span>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>
                                                    ФОРА2 по геймам (3) с кф за 1.64
                                                </span>
                                            </a>
                                            <span>от verniak01 17.06.2018</span>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>
                                                    ФОРА2 по геймам (3) с кф за 1.64
                                                </span>
                                            </a>
                                            <span>от verniak01 17.06.2018</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                    </article>

                </div>

                <div class="clr"></div>
                <div id="dialog-form"></div>

            </section>
            <!--noindex-->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>
    </div>

    <!-- upd 01.05.2018 -->

        <!-- Modal  -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts  -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script>
            var config = {
                type: 'line',
                data: {
                    labels: ["0", "7 дней", "14 дней", "30 дней"],
                    datasets: [{
                        label: '',
                        data: [0, 20, 60, 70, 90, 100],
                        backgroundColor: [
                            'rgba(255, 255, 255, 0)'
                        ],
                        borderColor: [
                            'rgba(88,93,209,1)'
                        ],
                        borderWidth: 2
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Победы'
                    },
                    legend: {
                        display: false,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: false
                    },
                    scales: {
                        xAxes: [{
                            display: false,
                            scaleLabel: {
                                display: false,
                                labelString: ''
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: false,
                                labelString: ''
                            },
                            ticks: {
                                min: 0,
                                max: 120,
                                stepSize: 30
                            }
                        }]
                    }
                }
            };

            window.onload = function() {
                var ctx = document.getElementById('graph01').getContext('2d');
                window.myLine = new Chart(ctx, config);
            };
        </script>

    </body>
</html>
