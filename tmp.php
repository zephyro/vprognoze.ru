<!doctype html>
<html lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>
        <div class="o-wrapper" id="o-wrapper">
        <div class="wrapper">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main clearfix">

                <!-- Sidebar Left -->
                <?php include('inc/sidebar_left.inc.php') ?>
                <!-- -->


                <div class="right_block_vp"><!-- header banner fonbet -->

                    <!-- SIDEBAR Right -->
                    <?php include('inc/sidebar_right.inc.php') ?>
                    <!-- -->

                    <article>

                        <div class="page">

                            <div class="news_boxing">
                                <div class="speedbar">
                                        <span id="dle-speedbar">
                                            <a href="#">Впрогнозе</a>
                                            »
                                            <a href="#">Прогнозы</a>
                                            »
                                            <a href="#">Ставки от профессионалов</a>
                                            »
                                            Прогнозы на хоккей
                                        </span>
                                </div>
                            </div>

                            <div class="page__box">
                                <div class="game">
                                    <div class="game__heading">
                                        <div class="game__heading_name">Теннис. Мировой тур ATP. Категория 250. Штутгарт, Германия. Одиночный разряд. 1/2 финала</div>
                                        <div class="game_heading_date">12.03.2015</div>
                                    </div>
                                    <ul class="game__meta">
                                        <li>
                                            <a href="#">
                                                <img src="images/game_meta_icon_01.png" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="images/game_meta_icon_02.png" alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>


                                <div class="box_wrap">
                                    <div class="box_heading">Способы оплаты</div>

                                </div>

                            </div>

                        </div>

                    </article>

                </div>

                <div class="clr"></div>
                <div id="dialog-form"></div>

            </section>
            <!--noindex-->

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>
    </div>

    <!-- upd 01.05.2018 -->

        <!-- Modal  -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->


        <!-- Scripts  -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
